import React from 'react';
import './App.css';
import RadioButton from "../../components/RadioButton/RadioButton";


const App = () => {
    return (
        <div className="App">
            <RadioButton />
        </div>
    );

};

export default App;
