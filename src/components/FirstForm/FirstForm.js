import React, {useState} from 'react';

const FirstForm = () => {

    const [state, setState] = useState({people: 0, price: 0, percent: 0, delivery: 0});

    const [totalPrice, setTotalPrice] = useState(0);
    const [eachTotal, setEachTotal] = useState(0);


    const onChangeHandler = e => {
        const stateCopy = {...state};
        stateCopy[e.target.name] = e.target.value;
        setState(stateCopy);
    };

    const onButtonHandler = () => {
        const total = parseInt(state.price) + (Math.ceil((parseInt(state.percent) / 100) * parseInt(state.price)) + parseInt(state.delivery));
        const each = Math.ceil(total / parseInt(state.people));
        setTotalPrice(total);
        setEachTotal(each);
    }

    if (isNaN(state.delivery)) {
        state.delivery = 0
    } else if(state.delivery === null) {
        state.delivery = 0
    }
    return (
        <div>
            <div>
                <p>Человек:&nbsp;
                    <input type="number"
                           name={'people'}
                           value={state.people}
                           onChange={(e) => onChangeHandler(e)}
                    /> чел.
                </p>
                <p>Сумма заказа:&nbsp;
                    <input type="number"
                           name={'price'}
                           value={state.price}
                           onChange={(e) => onChangeHandler(e)}
                    /> сом
                </p>
                <p>Процент чаевых:&nbsp;
                    <input type="number"
                           name={'percent'}
                           value={state.percent}
                           onChange={(e) => onChangeHandler(e)}
                    /> %
                </p>
                <p>Доставка:&nbsp;
                    <input type="number"
                           name={'delivery'}
                           value={state.delivery}
                           onChange={(e) => onChangeHandler(e)}
                    /> сом
                </p>
            </div>
            <button onClick={onButtonHandler}>Рассчитать</button>

            {totalPrice <= 0 ? null : <div>
                <p>Общая сумма: <strong>{totalPrice}</strong></p>
                <p>Количество человек: <strong>{state.people}</strong></p>
                <p>Каждый платит: <strong>{eachTotal}</strong></p>
            </div>}
        </div>
    );
};

export default FirstForm;